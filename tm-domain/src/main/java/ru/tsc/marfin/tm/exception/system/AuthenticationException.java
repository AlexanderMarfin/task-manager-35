package ru.tsc.marfin.tm.exception.system;

import ru.tsc.marfin.tm.exception.AbstractException;

public class AuthenticationException extends AbstractException {

    public AuthenticationException() {
        super("Error! Login or password is incorrect...");
    }

}
