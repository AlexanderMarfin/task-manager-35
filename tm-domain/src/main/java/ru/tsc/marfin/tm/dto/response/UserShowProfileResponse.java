package ru.tsc.marfin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserShowProfileResponse extends AbstractUserResponse {

    public UserShowProfileResponse(@Nullable final User user) {
        super(user);
    }

}
