package ru.tsc.marfin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.marfin.tm.command.AbstractCommand;


public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

}
